﻿using System;
using System.Collections.Generic;
using FileMonitoring.Interfaces;
using System.IO;
using System.Linq;
using System.Threading;

namespace FileMonitoring
{
	public class FileMonitor : IFileMonitor, IDisposable
	{
		private int _deltaCounter = 0;
		private IConfiguration _configuration;
		private FileSystemWatcher _fileWatcher;
		private string _twinDirPath;
		private List<SavedChangeInfo> _toDelete;
		private bool shouldTheMonitorFilter = true;
		private string[] fileExtensionsFilters = new[] { ".txt"};
		private bool _disposed = false;
		private List<SavedChangeInfo> _dateTimeToSavedChangeInfo;

        private class SavedChangeInfo
        {
			public string OldPath;
			public string NewPath;
			public DateTime DateTime;

			public SavedChangeInfo(string oldPath, string newPath, DateTime dateTime)
            {
				this.OldPath = oldPath;
				this.NewPath = newPath;
				this.DateTime = dateTime;
			}
		}


        public FileMonitor(IConfiguration configuration)
		{
			_toDelete = new List<SavedChangeInfo>();
			_dateTimeToSavedChangeInfo = new List<SavedChangeInfo>();
			 _configuration = configuration;
			_twinDirPath = Path.Combine(_configuration.BackupPath, "TwinDir");
			


			_fileWatcher = new FileSystemWatcher(_configuration.Path);
			_fileWatcher.NotifyFilter = NotifyFilters.Attributes
								 | NotifyFilters.CreationTime
								 | NotifyFilters.DirectoryName
								 | NotifyFilters.FileName
								 | NotifyFilters.LastAccess
								 | NotifyFilters.LastWrite
								 | NotifyFilters.Security
								 | NotifyFilters.Size;

			_fileWatcher.Changed += OnChange;

			_fileWatcher.EnableRaisingEvents = false;
		}

		~FileMonitor()
		{
			Dispose(false);
		}

		public void Stop()
		{
			Dispose();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
        {
			if (_disposed)
				return;

			_fileWatcher.EnableRaisingEvents = false;
			_fileWatcher.Dispose();
			DeleteBackUpDirIfItExists();

			if (disposing)
			{
                _configuration = null;
                _fileWatcher.Dispose();
                _fileWatcher = null;
                _twinDirPath = null;
                _toDelete = null;
                fileExtensionsFilters = null;
				_dateTimeToSavedChangeInfo = null;
			}

			_disposed = true;
		}

		public void Start()
		{
			ShakeOutBackupDir();
			StoreCurrentState();
			_fileWatcher.EnableRaisingEvents = true;
		}

		private void ShakeOutBackupDir()
        {
			DeleteBackUpDirIfItExists();
			Directory.CreateDirectory(_configuration.BackupPath);
		}

		private void DeleteBackUpDirIfItExists()
		{
			Thread.Sleep(100);
			if (Directory.Exists(_configuration.BackupPath))
				Directory.Delete(_configuration.BackupPath, true);
		}

        private void StoreCurrentState()
        {
            DirectoryInfo currentStateDirectory = new DirectoryInfo(_twinDirPath);

            if (currentStateDirectory.Exists)
                currentStateDirectory.Delete(true);

            new DirectoryInfo(_configuration.Path).CopyTo(currentStateDirectory);
        }

        private void OnChange(object sender, FileSystemEventArgs e)
		{
			string relativePath = Path.GetRelativePath(_configuration.Path, e.FullPath);
			string twinPath = Path.Combine(_twinDirPath, relativePath);
			string originalPath = e.FullPath;

			DeleteOldBrunchIfItExists();

			if (!shouldTheMonitorFilter || IsIncludedInFilters(Path.GetExtension(e.Name)))
			{
				DateTime timeOfChange = DateTime.Now;
				string ChagedFilePathToSave = Path.Combine(_configuration.BackupPath, _deltaCounter++.ToString());
				string oldFileContent = File.ReadAllText(twinPath);
				File.WriteAllText(ChagedFilePathToSave, oldFileContent);
				SavedChangeInfo savedChangeInfo = new SavedChangeInfo(originalPath, ChagedFilePathToSave, timeOfChange);
				_dateTimeToSavedChangeInfo.Add(savedChangeInfo);
			}
			File.Copy(e.FullPath, twinPath, true);
		}

		private bool IsIncludedInFilters(string extension) 
			=> fileExtensionsFilters.Any(filter => filter == extension);

		
		private void DeleteOldBrunchIfItExists()
        {
			if (_toDelete.Count == 0)
				return;

			_dateTimeToSavedChangeInfo = _dateTimeToSavedChangeInfo.Except(_toDelete).ToList();

			_toDelete = new List<SavedChangeInfo>();

			DirectoryInfo tempDirPath = new DirectoryInfo(Path.Combine(_configuration.BackupPath, "temp"));
			DirectoryInfo currentStateDir = new DirectoryInfo(_twinDirPath);
			tempDirPath.CopyTo(currentStateDir);
			tempDirPath.Delete(true);
        }

		public void Reset(DateTime dateTime)
        {
			_fileWatcher.EnableRaisingEvents = false;

			DirectoryInfo tempDir = new DirectoryInfo(Path.Combine(_configuration.BackupPath, "temp"));
            DirectoryInfo watchedDir = new DirectoryInfo(_configuration.Path);
            DirectoryInfo currentStateDir = new DirectoryInfo(_twinDirPath);
            DirectoryInfo backUpDir = new DirectoryInfo(_configuration.BackupPath);

			if (tempDir.Exists)
				tempDir.Delete(true);
			currentStateDir.CopyTo(tempDir);

			List<SavedChangeInfo> toApply = _dateTimeToSavedChangeInfo.Where(sci => sci.DateTime > dateTime)
				.OrderByDescending(sci => sci.DateTime).ToList();

			foreach(var sci in toApply)
            {
				string relativePath = Path.GetRelativePath(_configuration.Path, sci.OldPath);
				string pastePath = Path.Combine(tempDir.FullName, relativePath);
				File.Copy(sci.NewPath, pastePath, true);
				_toDelete.Add(sci);
            }

			watchedDir.Delete(true);
			tempDir.CopyTo(watchedDir);

			_fileWatcher.EnableRaisingEvents = true;
		}
	}

	public static class DirectoryInfo_Extensions
	{
		public static void CopyTo(this DirectoryInfo source, DirectoryInfo target)
		{
			Directory.CreateDirectory(target.FullName);

			source.Refresh();
            if (!source.Exists)
                throw new Exception("Copy exception. dir does not exist!");

            foreach (FileInfo file in source.GetFiles())
            {
				file.Refresh();
				if (!source.Exists)
					throw new Exception("Copy exception. file does not exist!");
				file.CopyTo(Path.Combine(target.FullName, file.Name), true);
			}
			foreach (DirectoryInfo subDir in source.GetDirectories())
			{
				DirectoryInfo nextTargetSubDir =
					target.CreateSubdirectory(subDir.Name);
				CopyTo(subDir, nextTargetSubDir);
			}
		}

		public static void DeleteDirectory(string path)
		{
			foreach (string directory in Directory.GetDirectories(path))
			{
				DeleteDirectory(directory);
			}

			try
			{
				Directory.Delete(path, true);
			}
			catch (IOException)
			{
				Directory.Delete(path, true);
			}
			catch (UnauthorizedAccessException)
			{
				Directory.Delete(path, true);
			}
		}
	}
}
