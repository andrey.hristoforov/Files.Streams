﻿using System;
using System.Threading;
using System.IO;
using System.Linq;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using FileMonitoring.Interfaces;

namespace FileMonitoring
{
	[TestFixture, NonParallelizable]
	public class FileMonitoring_Tests
	{
		private static Mock<IConfiguration> _configurationMock;

		private static string _backUpDirPath = @"C:\Users\andre\Desktop\типа гит\bu";
		private static string _toMonitoringDirPath = @"C:\Users\andre\Desktop\типа гит\dir";

		private FileMonitor currentFileMonitor;

		[OneTimeSetUp]
		public void OneTimesUp()
		{
			_configurationMock = new Mock<IConfiguration>();
			_configurationMock.Setup(c => c.BackupPath).Returns(_backUpDirPath);
			_configurationMock.Setup(c => c.Path).Returns(_toMonitoringDirPath);

			if (new DirectoryInfo(_toMonitoringDirPath).Exists)
				new DirectoryInfo(_toMonitoringDirPath).Delete(true);

			new DirectoryInfo(_toMonitoringDirPath).Create();

		}

        [TearDown]
        public void TearDown()
        {
            if (currentFileMonitor != null)
                currentFileMonitor.Dispose();

            Thread.Sleep(1000);
            Directory.Delete(_toMonitoringDirPath, true);
            Directory.CreateDirectory(_toMonitoringDirPath);
			Thread.Sleep(1000);
		}

        [Test]
		public void FileContentChange_SimpleTest()
		{
			string filePath = Path.Combine(_toMonitoringDirPath, "NaKoleniTolkoPeredBogom.txt");

			File.Create(filePath).Close();
			Thread.Sleep(500);

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();

			File.AppendAllLines(filePath, new[] { "Is this the real life? Is this just fantasy?" });
			string expectedContent = File.ReadAllText(filePath);
			Thread.Sleep(500);

			DateTime controlPoint1 = DateTime.Now;
			File.AppendAllLines(filePath, new[] { "Caught in a landslide, no escape from reality..." });
			Thread.Sleep(500);
			fileMonitor.Reset(controlPoint1);

			string actualContent = File.ReadAllText(filePath);
			Assert.AreEqual(expectedContent, actualContent);
		}

		[Test]
		public void FileContentChange_MoreDifficultTest()
        {
			string firstFilePath = Path.Combine(_toMonitoringDirPath, "file1.txt");
			string secondFilePath = Path.Combine(_toMonitoringDirPath, "file2.txt");
			File.Create(firstFilePath).Close();
			File.Create(secondFilePath).Close();
			Thread.Sleep(500);

			List<DateTime> controlPoints = new List<DateTime>();
			List<string[]> expectedContens = new List<string[]>();
			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);//0
			controlPoints.Add(DateTime.Now);
			expectedContens.Add(new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) });

			File.AppendAllLines(firstFilePath, new[] { "Open your eyes, look up to the skies and see" });
			File.AppendAllLines(secondFilePath, new[] { "I'm just a poor boy, I need no sympathy" });
			Thread.Sleep(500);//1
			controlPoints.Add(DateTime.Now);
			expectedContens.Add(new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) });

			File.AppendAllLines(firstFilePath, new[] { "Because I'm easy come, easy go, little high, little low" });
			File.AppendAllLines(secondFilePath, new[] { "Any way the wind blows doesn't really matter to me, to me" });
			Thread.Sleep(500);//2
			controlPoints.Add(DateTime.Now);
			expectedContens.Add(new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) });

			File.AppendAllLines(firstFilePath, new[] { "Mama, just killed a man",
				"Put a gun against his head, pulled my trigger, now he's dead",
				"Mama, life had just begun",
				"But now I've gone and thrown it all away"});
			Thread.Sleep(500);//3
			controlPoints.Add(DateTime.Now);
			expectedContens.Add(new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) });

			File.AppendAllLines(secondFilePath, new[] { "Mama, ooh, didn't mean to make you cry",
				"If I'm not back again this time tomorrow",
				"Carry on, carry on",
				"As if nothing really matters"});


			Thread.Sleep(500);
			fileMonitor.Reset(controlPoints[3]);
			string[] actualContent = new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) };
			Assert.AreEqual(expectedContens[3], actualContent);

			fileMonitor.Reset(controlPoints[2]);
			actualContent = new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) };
			Assert.AreEqual(expectedContens[2], actualContent);


			File.AppendAllLines(firstFilePath, new[] { "Too late, my time has come" });
			File.AppendAllLines(secondFilePath, new[] { "Sends shivers down my spine, body's aching all the time" });
			Thread.Sleep(1000);//4
			controlPoints.Add(DateTime.Now);
			expectedContens.Add(new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) });

			File.AppendAllLines(firstFilePath, new[] { "Goodbye, everybody, I've got to go" });
			File.AppendAllLines(secondFilePath, new[] { "Gotta leave you all behind and face the truth" });
			Thread.Sleep(500);


			fileMonitor.Reset(controlPoints[4]);
			actualContent = new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) };
			Assert.AreEqual(expectedContens[4], actualContent);

			fileMonitor.Reset(controlPoints[2]);
			actualContent = new[] { File.ReadAllText(firstFilePath), File.ReadAllText(secondFilePath) };
			Assert.AreEqual(expectedContens[2], actualContent);
		}

		[Test]
		public void FileRename_SimpleTest()
        {
			string filePath = Path.Combine(_toMonitoringDirPath, "file.txt");
			FileInfo file = new FileInfo(filePath);
			file.Create().Close();
			Thread.Sleep(500);
			DateTime controlPoint = DateTime.Now;

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			string renamedFilePath = Path.Combine(file.DirectoryName, "definitelyNotAFile.txt");
			File.Move(filePath, renamedFilePath);
			Thread.Sleep(500);

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

			Assert.IsTrue(Directory.GetFiles(_toMonitoringDirPath).Contains(filePath));
			Assert.IsFalse(Directory.GetFiles(_toMonitoringDirPath).Contains(renamedFilePath));

			return;
		}
		//добавить сложный тест ренейма файла
		[Test]
		public void FileDelete_SimpleTest()
		{
			string filePath = Path.Combine(_toMonitoringDirPath, "file.txt");
			FileInfo file = new FileInfo(filePath);
			file.Create().Close();
			Thread.Sleep(500);
			DateTime controlPoint = DateTime.Now;

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			File.Delete(filePath);
			Thread.Sleep(500);

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

			Assert.IsTrue(Directory.GetFiles(_toMonitoringDirPath).Contains(filePath));

			return;
		}
		//добавить сложный тест удаления файла файла

		[Test]
		public void FileCreate_SimpleTest()
		{
			string filePath = Path.Combine(_toMonitoringDirPath, "file.txt");
			FileInfo file = new FileInfo(filePath);
			DateTime controlPoint = DateTime.Now;

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			file.Create().Close();
			Thread.Sleep(500);

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

			Assert.IsFalse(Directory.GetFiles(_toMonitoringDirPath).Contains(filePath));

			return;
		}


        //добавить сложный тест создания файла файла

        [Test]
        public void aboba()
        {
			string directoryPath = Path.Combine(_toMonitoringDirPath, "dir");
			string filePath = Path.Combine(directoryPath, "file.txt");
			DirectoryInfo directory = new DirectoryInfo(directoryPath);
			FileInfo file = new FileInfo(filePath);
			directory.Create();
			file.Create().Close();

			Thread.Sleep(500);
			DateTime controlPoint = DateTime.Now;

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			directory.Delete(true);
			Thread.Sleep(500);

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

			Assert.IsTrue(directory.Exists);
			Assert.IsTrue(file.Exists);

			return;
		}

		[Test]
		public void DirectoryRename_SimpleTest()
		{
			string directoryPath = Path.Combine(_toMonitoringDirPath, "dir");
			DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
			directoryInfo.Create();
			Thread.Sleep(500);
			DateTime controlPoint = DateTime.Now;

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			string renamedDirPath = Path.Combine(_toMonitoringDirPath, "AnotherDirName");
			directoryInfo.MoveTo(renamedDirPath);
			Thread.Sleep(500);

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

            Assert.IsTrue(Directory.GetDirectories(_toMonitoringDirPath).Contains(directoryPath));
            Assert.IsFalse(Directory.GetDirectories(_toMonitoringDirPath).Contains(renamedDirPath));
		}
		[Test]
		public void DirectoryRename_MoreDifficult()
		{
			string directoryPath = Path.Combine(_toMonitoringDirPath, "dir");
			string subDirPath = Path.Combine(directoryPath, "subDir");
			string filePath = Path.Combine(directoryPath, "file.txt");
			string fileInSubDirPath = Path.Combine(subDirPath, "fileInSubDir.txt");
			DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
			DirectoryInfo subDirInfo = new DirectoryInfo(subDirPath);
			FileInfo fileInfo = new FileInfo(filePath);
			FileInfo fileInSubDirInfo = new FileInfo(fileInSubDirPath);
			directoryInfo.Create();
			subDirInfo.Create();
			fileInfo.Create().Close();
			fileInSubDirInfo.Create().Close();
			File.AppendAllLines(filePath, new[] { "Mama, ooh (Any way the wind blows)" });
			File.AppendAllLines(fileInSubDirPath, new[] { "I don't wanna die, I sometimes wish I'd never been born at all" });
			Thread.Sleep(500);
			DateTime controlPoint = DateTime.Now;
			string expectedFileContent = File.ReadAllText(filePath);
			string expectedFileInSubDirContent = File.ReadAllText(fileInSubDirPath);

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			string renamedDirPath = Path.Combine(_toMonitoringDirPath, "AnotherDirName");
			directoryInfo.MoveTo(renamedDirPath);
			Thread.Sleep(500);

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

			Assert.IsFalse(Directory.GetDirectories(_toMonitoringDirPath).Contains(renamedDirPath));
			Assert.IsTrue(Directory.GetDirectories(_toMonitoringDirPath).Contains(directoryPath));

			Assert.IsTrue(Directory.Exists(subDirPath));
			Assert.IsTrue(File.Exists(filePath));
			Assert.AreEqual(expectedFileContent, File.ReadAllText(filePath));

			Assert.IsTrue(File.Exists(fileInSubDirPath));
			Assert.AreEqual(expectedFileInSubDirContent, File.ReadAllText(fileInSubDirPath));
		}
		[Test]
        public void DirectoryDelete_SimpleTest()
        {
			string directoryPath = Path.Combine(_toMonitoringDirPath, "dir");
			DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
			directoryInfo.Create();
			Thread.Sleep(500);
			DateTime controlPoint = DateTime.Now;

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			directoryInfo.Delete(true);
			Thread.Sleep(500);

			Assert.IsFalse(Directory.GetDirectories(_toMonitoringDirPath).Contains(directoryPath));

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

			Assert.IsTrue(Directory.GetDirectories(_toMonitoringDirPath).Contains(directoryPath));
		}
		[Test]
		public void DirectoryDelete_MoreDifficult()
		{
			string directoryPath = Path.Combine(_toMonitoringDirPath, "dir");
			string subDirPath = Path.Combine(directoryPath, "subDir");
			string filePath = Path.Combine(directoryPath, "file");
			string fileInSubDirPath = Path.Combine(subDirPath, "fileInSubDir");
			DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
			DirectoryInfo subDirInfo = new DirectoryInfo(subDirPath);
			FileInfo fileInfo = new FileInfo(filePath);
			FileInfo fileInSubDirInfo = new FileInfo(fileInSubDirPath);
			directoryInfo.Create();
			subDirInfo.Create();
			fileInfo.Create().Close();
			fileInSubDirInfo.Create().Close();
			File.AppendAllLines(filePath, new[] { "I see a little silhouetto of a man" });
			File.AppendAllLines(fileInSubDirPath, new[] { "Scaramouche, Scaramouche, will you do the Fandango?" });
			Thread.Sleep(500);
			DateTime controlPoint = DateTime.Now;
			string expectedFileContent = File.ReadAllText(filePath);
			string expectedFileInSubDirContent = File.ReadAllText(fileInSubDirPath);

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			directoryInfo.Delete(true);
			Thread.Sleep(500);

			Assert.False(Directory.GetDirectories(_toMonitoringDirPath).Contains(directoryPath));
			Assert.False(Directory.Exists(subDirPath));
			Assert.False(File.Exists(filePath));
			Assert.False(File.Exists(fileInSubDirPath));

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

			Assert.IsTrue(Directory.GetDirectories(_toMonitoringDirPath).Contains(directoryPath));

			Assert.IsTrue(Directory.Exists(subDirPath));
			Assert.IsTrue(File.Exists(filePath));
			Assert.AreEqual(expectedFileContent, File.ReadAllText(filePath));

			Assert.IsTrue(File.Exists(fileInSubDirPath));
			Assert.AreEqual(expectedFileInSubDirContent, File.ReadAllText(fileInSubDirPath));
		}

        [Test]
        public void DirectoryCreate_SimpleTest()
        {
			string directoryPath = Path.Combine(_toMonitoringDirPath, "dir");
			DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
			Thread.Sleep(500);
			DateTime controlPoint = DateTime.Now;

			FileMonitor fileMonitor = new FileMonitor(_configurationMock.Object);
			currentFileMonitor = fileMonitor;
			fileMonitor.Start();
			Thread.Sleep(500);

			directoryInfo.Create();
			Thread.Sleep(500);

			Assert.IsTrue(Directory.GetDirectories(_toMonitoringDirPath).Contains(directoryPath));

			fileMonitor.Reset(controlPoint);
			Thread.Sleep(500);

			Assert.IsFalse(Directory.GetDirectories(_toMonitoringDirPath).Contains(directoryPath));
		}
    }
}
